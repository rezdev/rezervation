'use strict';

var mongoskin = require('mongoskin');
var db = mongoskin.db('mongodb://@localhost:' + config.db_port + '/' + config.db, {safe:true}); // jshint ignore:line

var collectionURL = '/api/tickets/';

describe('Api routes', function(){

  var id;
  var data = {
    name: 'Saint Pantaleon',
    barcode: '1234567890123456',
    quantity: 5,
    seat: '2a'
  };
  // for use when updating the ticket later
  var newName = 'St Pantaleon';

  before(function(){
    db.bind('tickets').drop();
  });

  it('posts object', function(done){
    request
      .post(collectionURL)
      .send(data)
      .expect(200)
      .end(function(e,res){
        expect(res.body.length).to.eql(1);
        // data tests
        var user = res.body[0];
        //@TODO check if the quantity was coerced into a number if given a string
        expect(user).to.contain(data);
        expect(user._id.length).to.eql(24);
        id = user._id;
        done();
      });
  });

  it('reads collection of objects', function(done) {
    request
      .get(collectionURL)
      .end(function(e, res) {
        expect(res.body.length).to.be.above(0);
        done();
      });
  });

  it('reads an object', function(done) {
    request
      .get(collectionURL + id)
      .expect(200)
      .end(function(e, res) {
        var user = res.body;
        expect(user._id).to.be.eql(id);
        expect(user).to.contain(data);
        done();
      });
  });

  it('updates an object', function(done) {
    request
      .put(collectionURL+ id)
      .send({
        name: newName
      })
      .expect(200, done);
  });

  it('reads an updated object', function(done) {
    request
      .get(collectionURL + id)
      .expect(200)
      .end(function(e, res) {
        var user = res.body;
        expect(user._id).to.be.eql(id);
        expect(user.name).to.be.eql(newName);
        done();
      });
  });

  it('deletes an object', function(done) {
    request
      .del(collectionURL+ id)
      .expect(200, done);
  });

  it('does not read a deleted object', function(done) {
    request
      .get(collectionURL + id)
      .expect(200, done); // @TODO should be a 404
  });

  describe('checkin route', function () {
    var barcode = '2934725023745023';
    var barcode0Qty = '1234123412342134';
    var barcodeWrongService = '4321432143214321';
    var ticketService = '12:00PM';
    beforeEach(function(done) {
      db.tickets.insert([
        {
          name: 'Saint Pantaleon',
          barcode: barcode,
          quantity: 7,
          seat: '2b'
        },
        {
          name: 'Saint Cecilia',
          barcode: barcode0Qty,
          quantity: 0,
          seat: '2c'
        },
        {
          name: 'Saint Eligius',
          barcode: barcodeWrongService,
          quantity: 3,
          seat: '2f',
          service: ticketService
        }
      ], {}, done);
    });

    afterEach(function (done) {
      db.tickets.remove({}, done);
    });

    it('decrements a barcode\'s ticket quantity and returns a human readable msg', function (done) {
      request
        .post(collectionURL + 'checkin/' + barcode)
        .expect(200)
        .expect({
          quantity: 6,
          msg: '6 tickets remaining for ' + barcode
        }, done);
    });

    it('returns an error if the barcode doesn\'t exist', function(done){
      request
        .post(collectionURL + 'checkin/' + 'foobarcode')
        .expect(404)
        .expect(/Barcode foobarcode not found/, done);
    });

    it('returns an error if the barcode\'s tickets have already been used', function(done) {
      request
        .post(collectionURL + 'checkin/' + barcode0Qty)
        .expect(400)
        .expect({
          msg: 'No more tickets available for barcode: ' + barcode0Qty // @TODO: Display original amount of tickets reserved in error msg
        }, done);
    });

    it('returns an error if the barcode is for the wrong service', function(done){
      var scanService = '3:00PM';
      request
        .post(collectionURL + 'checkin/' + barcodeWrongService)
        .send({ service: scanService })
        .expect(400)
        .expect({
          msg: 'Barcode: ' + barcodeWrongService + ' is for the ' + ticketService + ' service, you are scanning for the ' + scanService + ' service.'
        }, done);
    });
  });
});
