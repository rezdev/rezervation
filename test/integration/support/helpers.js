'use strict';

module.exports = {
  login: function (username, password) {
    username = username || 'test';
    password = password || 'test';
    browser.get('/login');

    $('#username').sendKeys(username);
    $('#password').sendKeys(password);
    $('button').click();
  },
  logout: function () {
    browser.manage().deleteAllCookies();
  }
};
