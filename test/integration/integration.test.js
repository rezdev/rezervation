'use strict';

var helpers = require('./support/helpers');

browser.ignoreSynchronization = true;

describe('Rezervation', function () {

  describe('Checkin process', function () {

    it('allows a user to check a barcode in', function () {
      helpers.login();
      $('#ticket_number').sendKeys('11111111');
      $('button').click();

      expect($('.alert-success').isPresent()).toBeTruthy();
    });

    it('notifies a user if a barcode is not found', function () {
      var barcode = 'fobarbaz';
      $('#ticket_number').sendKeys(barcode);
      $('button').click();

      expect($('.alert-warning').getText()).toEqual('Barcode ' + barcode + ' not found');
    });

    it('does not checkins for tickets without any left', function () {

      $('#ticket_number').sendKeys('00000000');
      $('button').click();

      expect($('.alert-warning').isPresent()).toBeTruthy();
    });

    it('does not checkin for barcodes for a different service time', function () {
      element(by.cssContainingText('option', '3:00pm')).click()
      $('#ticket_number').sendKeys('44444444');

      expect($('.alert-warning').isPresent()).toBeTruthy();
    });

    it('does not display more than 3 notifications', function () {
      [1,2,3,4].forEach(function () {
        $('#ticket_number').sendKeys('00000000');
        $('button').click();
      });

      browser.sleep(1000);
      expect($$('.alert').count()).toBe(3);
    });
  });

  describe('checkin page', function () {

    it('redirects user to login page if not logged in', function () {
      helpers.logout()
      browser.get('/');
      expect(browser.getTitle()).toEqual('Login');
      expect(browser.getCurrentUrl()).toContain('/login');
    });
  });

  describe('Logging in to Rezervation', function () {
    beforeEach(helpers.logout);

    it('logs in a user with correct credentials', function () {
      helpers.login();
      expect(browser.getTitle()).toEqual('REZervation Checkin');
    });

    it('with incorrect credentials does not log in a user', function () {
      helpers.login('badUsername', 'badPassword');
      expect(browser.getTitle()).toEqual('Login');
    });
  });
});
