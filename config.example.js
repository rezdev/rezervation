var config = {};

// --------------------------------------------------
// CHANGE THESE VALUES
// --------------------------------------------------
config.username = 'YOUR_USERNAME';
config.password = 'YOUR_PASSWORD';
config.db       = 'YOUR_MONGODB_DATABASE_NAME';
config.secret   = 'YOUR_SECRET';
config.port     = '3000';
config.db_port  = '27017';
// --------------------------------------------------
// CHANGE NOTHING BELOW HERE
// --------------------------------------------------

if(process.env.NODE_ENV === 'test') {
  config.username = 'test';
  config.password = 'test';
  config.db = config.db + '-test';
}

module.exports = config;
