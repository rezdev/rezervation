module.exports = {
  isLoggedIn: function (req, res, next) {
    // For convenience when testing api calls, isLoggedIn is ignored
    if(process.env.NODE_ENV == 'test' && req.baseUrl == '/api')
      return next();

    if (req.isAuthenticated()) {
      return next();
    }

    if (req.xhr) {
      res.status(403).json({
        msg: 'Please refresh your browser and login to continue'
      });
    } else {
      res.redirect('/login');
    }
  }
};
