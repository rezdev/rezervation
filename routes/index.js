'use strict';
var helper = require('./helper');

module.exports = function(app, passport) {
  app.get('/', helper.isLoggedIn, function(req, res) {
    res.render('index', {
      title: 'REZervation Checkin'
    });
  });

  app.get('/login', function(req, res) {
    res.render('login', {
      title: 'Login',
      message: req.flash('error')
    });
  });

  app.post('/login', passport.authenticate('local', {
      successRedirect: '/',
      failureRedirect: '/login',
      failureFlash: true
  }));
};
