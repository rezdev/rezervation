var router = require('express').Router();
var helper = require('./helper');

// Mongo params
router.param('collectionName', function(req, res, next, collectionName){
  req.collection = req.db.collection(collectionName);
  return next();
});

// GET all – Read
router.get('/:collectionName', helper.isLoggedIn, function(req, res, next) {
  req.collection.find({} ,{limit:10, sort: [['_id',-1]]}).toArray(function(e, results){
    if (e) { return next(e); }
    res.send(results);
  });
});

// GET – Single read
router.get('/:collectionName/:id', helper.isLoggedIn, function(req, res, next) {
  req.collection.findById(req.params.id, function(e, result){
    if (e) { return next(e); }
    res.send(result);
  });
});

// POST – Create
router.post('/:collectionName', helper.isLoggedIn, function(req, res, next) {
  // Coerce qty into an integer if it's truthy.
  if ( req.body.quantity ) {
    req.body.quantity = parseInt( req.body.quantity );
  }

  req.collection.insert(req.body, {}, function(e, results){
    if (e) { return next(e); }
    res.send(results);
  });
});

// PUT – Update
router.put('/:collectionName/:id', helper.isLoggedIn, function(req, res, next) {
  req.collection.updateById(req.params.id, {$set:req.body}, {safe:true, multi:false}, function(e, result){
    if (e) { return next(e); }
    res.send((result===1)?{msg:'success'}:{msg:'error'});
  });
});

// DELETE – Removes an entry from the collection
router.delete('/:collectionName/:id', helper.isLoggedIn, function (req, res, next) {
  req.collection.removeById(req.params.id, function(e, result) {
    if (e) { return next(e); }
    res.send((result===1)?{msg:'success'}:{msg:'error'});
  });
});

// POST – Check-in ticket AKA decrements ticket quantity
router.post('/:collectionName/checkin/:barcode', helper.isLoggedIn, function (req, res, next) {
  req.collection.findAndModify({ barcode: req.params.barcode, quantity: {$gt: 0}, service: req.body.service },
    {},
    {$inc: {quantity: -1}}, {new: true, safe: true },
    function (error, result) {
      if (error) {
        return next(error);
      }

      if (result) {
        res.send({
          quantity: result.quantity,
          msg: result.quantity + ' tickets remaining for ' + req.params.barcode
        });
      } else {
        // If no result is found, lets check for reasons why
        req.collection.findOne({ barcode: req.params.barcode }, function (error, result) {
          if (error) {
            return next(error);
          }

          // Barcode doesn't exist
          if ( ! result ) {
            res.status(404).send({
              msg: 'Barcode ' + req.params.barcode + ' not found'
            });
          } else {
            // There is not enough quantity left
            if ( result.quantity === 0 ) {
              res.status(400).send({
                msg: 'No more tickets available for barcode: ' + req.params.barcode
              });
            // Result has the wrong service
            } else if ( result.service !== req.params.service ) {
              res.status(400).send({
                msg: 'Barcode: ' + req.params.barcode + ' is for the ' + result.service + ' service, you are scanning for the ' + req.body.service + ' service.'
              });
            } else {
              res.status(500).end();
            }
          }
        });
      }
    });
});

module.exports = router;
