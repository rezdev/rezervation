#!/usr/bin/env node

var config = require('../config'),
    csv = require('csv'),
    fs = require('fs'),
    Q = require('q'),
    chalk = require('chalk'),
    mongoskin = require('mongoskin'),
    db = mongoskin.db('mongodb://@localhost:' + config.db_port + '/' + config.db, {safe:true});

var dump = process.argv[2] || 'dump.csv';

db.bind('tickets');

db.tickets.drop();

fs.readFile(dump, function(err, data) {
  if ( err ) {
    return console.error(err);
  }

  // File read successfully Continue
  csv.parse(data, function(err, data){
    // an array of arrays is returned as data

    // Get the header row to identify the column for each bit of data
    var headerRow = data.shift();

    var firstNameCol  = headerRow.indexOf('First Name'),
        lastNameCol   = headerRow.indexOf('Last Name'),
        barcodeCol    = headerRow.indexOf('Confirmation Code'),
        quantityCol   = headerRow.indexOf('Number Seats'),
        sectionCol    = headerRow.indexOf('Section'),
        serviceCol    = headerRow.indexOf('Service');

    var insertCount = 0;

    var dataPromises = data.map(function(row) {
      return Q.Promise(function (resolve, reject) {
        // map between row columns and db document structure
        var rowInsert = {
          name: row[firstNameCol].trim() + ' ' + row[lastNameCol].trim(),
          barcode: row[barcodeCol].trim(),
          // Parse quantity to ensure it is a integer
          quantity: parseInt( row[quantityCol].trim() ),
          section: row[sectionCol].trim(),
          service: row[serviceCol].trim()
        };

        // Skip tickets without a barcode, quantity, or service time
        var errMessage;
        if ( ! rowInsert.barcode ) {
          errMessage = chalk.yellow( '∆' ) + ' No Barcode found – skipped';
        } else if ( ! rowInsert.quantity ) {
          errMessage = chalk.yellow( '∆' ) + ' No Quantity found for barcode ' + rowInsert.barcode + ' – skipped';
        } else if ( ! rowInsert.service ) {
          errMessage = chalk.yellow( '∆' ) + ' No Service found for barcode ' + rowInsert.barcode + ' – skipped';
        }

        if(errMessage) {
          console.log(errMessage);
          return resolve(errMessage);
        }


        // Insert new data
        db.tickets.insert(rowInsert, {}, function(error, result) {
          if (error) {
            reject(error);
            return console.error(error);
          }
          if (result) {
            console.log(chalk.green( '✓' ) + ' Barcode [ ' + rowInsert.barcode + ' ] – inserted');
            insertCount ++;
            resolve();
          } else {
            var errMessage = chalk.red( '✘' ) + ' Barcode [ ' + rowInsert.barcode + ' ] – failed to insert';
            reject(errMessage);
            console.log(errMessage);
          }
        });
      });
    });

    // Wait for things to settle down
    Q.all(dataPromises).finally(function() {
      console.log(insertCount + ' Tickets imported');
      db.close()
    });
  });
});
