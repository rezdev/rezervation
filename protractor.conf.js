'use strict';

var seed = require('./scripts/seed');
var config = require('./config');

exports.config = {
  directConnect: true,

  baseUrl: 'http://localhost:' + config.port,

  capabilities: {
    'browserName': 'chrome'
  },

  specs: ['./test/integration/integration.test.js'],

  beforeLaunch: function() {
    seed()
  },

  jasmineNodeOpts: {
    showColors: true,
  }
};
