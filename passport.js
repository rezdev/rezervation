var LocalStrategy = require('passport-local').Strategy;
var config = require('./config');

module.exports = function(passport) {

  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(id, done) {
    done(null, id);
  });

  passport.use(new LocalStrategy(
    function(username, password, done) {
      if(username === config.username && password === config.password) {
        return done(null, username);
      } else {
        return done(null, false, { message: 'Incorrect Username and Password' });
      }
    }
  ));
};

