'use strict';

var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var concat = require('gulp-concat');

var DIST_DIR = 'public/';
var MANIFEST = {
  js: {
    all: './src/**/*.js',
    src: './src/app.js',
    name: 'app.js',
    dest: 'js'
  },
  css: {
    src: [
      'node_modules/bootstrap/dist/css/bootstrap.min.css',
      'src/css/*.css'
    ],
    name: 'app.css',
    dest: 'css'
  }
};

function bundle (src) {
  return browserify(src)
  // transforms here
  .bundle();
}

gulp.task('js', function () {
  return bundle(MANIFEST.js.src)
  .pipe(source(MANIFEST.js.name))
  .pipe(gulp.dest(DIST_DIR + MANIFEST.js.dest));
});

gulp.task('css', function () {
  return gulp.src(MANIFEST.css.src)
  .pipe(concat(MANIFEST.css.name))
  .pipe(gulp.dest(DIST_DIR + MANIFEST.css.dest));
});

gulp.task('watch', function () {
 gulp.watch([MANIFEST.js.all, MANIFEST.css.src], ['js', 'css']);
});

gulp.task('build', ['js', 'css']);
gulp.task('default', ['build', 'watch']);
