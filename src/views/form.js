'use strict';

var $ = require('jquery');
var ENDPOINT = '/api/tickets/';

function Form () {
  this.$view = $('.ticket-processing');
  this.$form = this.$view.find('form');
  this.$barcode = this.$form.find('#ticket_number');
  this.$service = this.$form.find('#service');

  this.$successCheckbox = this.$form.find('#success_sound');
  this.$failCheckbox = this.$form.find('#failure_sound');
  this.failSound = new Audio('/audio/failure.wav');
  this.successSound = new Audio('/audio/success.wav');

  this.$form.on('submit', this.handleSubmit.bind(this));
}

Form.prototype.handleSubmit = function (e) {
  e.preventDefault();

  var req = $.ajax({
    method: 'POST',
    url: ENDPOINT + 'checkin/' + this.$barcode.val(),
    data: { service: this.$service.val() },
    dataType: 'json'
  });

  req.done(function (body) {
    if(this.$successCheckbox.is(':checked'))
      this.successSound.play();

    this.broadcast(body, 200);
  }.bind(this));

  req.fail(function (xhr) {
    if(this.$failCheckbox.is(':checked'))
      this.failSound.play();

    var content = xhr.responseJSON || xhr.responseText;
    this.broadcast(content, xhr.status);
  }.bind(this));

  this.$barcode.focus();
  this.$barcode.val('');
};

Form.prototype.broadcast = function (content, status) {
  $(document).trigger('checkin', {
    body: content,
    status: status
  });
}

module.exports = Form;
