'use strict';

var $ = require('jquery');

function template (content, type) {
  return $('<div class="alert alert-'+ type + '">' + content + '<div>');
};

function Notification () {
  this.$view = $('.ticket-processing-info');
  this.notifications = [];
}

Notification.prototype.add = function (type, content) {
  type = type || 'info';
  var newNotification = template(content, type);

  this.$view.find('.alert').eq(2).fadeOut(1000, function () {
    $(this).remove();
  });
  this.$view.prepend(newNotification.hide());
  this.$view.find('.alert').eq(0).slideDown('fast');
};

module.exports = Notification;
