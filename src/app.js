'use strict';

var $ = require('jquery');
var Notification = require('./views/notification');
var FormView = require('./views/form');
var STATUS_MAP = {
  200: {
    type: 'success',
    message: 'Success'
  },
  400: {
    type: 'warning',
    message: 'No tickets available'
  },
  403: {
    type: 'danger',
    message: 'Please refresh your browser and login to continue'
  },
  404: {
    type: 'warning',
    message: 'Ticket not found'
  },
  500: {
    type: 'danger',
    message: 'Server Error'
  }
};

var form = new FormView();
var notification = new Notification();

$(document).on('checkin', function (event, data) {
  var content;
  var statusInfo = STATUS_MAP[data.status];
  var type = statusInfo.type || 'info';
  if (data.body.msg) {
    content = data.body.msg;
  } else {
    content = statusInfo.message || data.statusCode;
  }

  notification.add(type, content);
});
