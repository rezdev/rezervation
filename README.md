# Installation

```shell
# make sure you have homebrew installed http://brew.sh/
brew install mongodb
npm i

cp config.example.js config.js
# fill in your username and password in the config.js file
```

# Starting the application

```shell
npm start
```

# Tests

```shell
# this only needs to be run ocassionally to keep the driver up to date
npm run setup-protractor

# For integration tests, the app needs to be started in test environment. Do this in a separate terminal window
NODE_ENV=test npm start
npm test
```

# Importing

You can import a csv file into the db by running:

```sh
npm run import path/to/csvfile.csv
```

If no file is specified, it'll default to `dump.csv` in the root of the directory.
