var mongoskin = require('mongoskin'),
    config = require('../config');

var BARCODE_LENGTH = 8;
var COLLECTION = 'tickets';
// not sure if want to force this to test or not. We shouldn't be seeding in prod anyway.
var db = mongoskin.db('mongodb://@localhost:27017/' + config.db, {safe:true});

var tickets = ['0', '1', '2', '3', '4', '5'].map(function (number) {
  return {
    barcode: new Array(BARCODE_LENGTH + 1).join(number),
    service: '12:00pm',
    quantity: parseInt(number, 10)
  }
});


function seed (cb) {
  cb = cb || function () {};
  console.log('Emptying', COLLECTION);
  db.collection(COLLECTION).remove({}, function (err) {
    if (err) throw new Error(err);

    console.log('Creating new', COLLECTION);
    tickets.forEach(function (ticket, index) {
      db.collection(COLLECTION).insert(ticket, {}, function (e, result) {
        // this could probably be handled more elegantly
        if (index === tickets.length -1) {
          db.close();
          cb();
        }
      });
    });
  });
}

// execute seed if we are being run directly
// https://nodejs.org/api/modules.html#modules_accessing_the_main_module
if (require.main === module) {
  seed()
}

module.exports = seed;
